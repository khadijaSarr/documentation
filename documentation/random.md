# Random

## Rappels:
Pour activer un script, il faut rappeler le script lui meme dans le fichier ``.html`` par exemple:
````
<script src="object.js" type="module"></script>
````

## Bibliothèque Math:
 C'est une bibliothèque qui nous est fourni par javasript qui nous permet de faire des opérations mathématiques.
 Ce qui est important à retenir c'est que javascript est un langage qui est très nul en math, s'il ya des opérations 
 à faire vaut mieux ne pas les faire en javascript ni en node.
 
**Voici un exemple des limites de JS en Math:**
 
 ![opérationMath](/uploads/4e9c94eed2ef9091ca924c057b81b367/opérationMath.PNG)
 
 et le résultat est:
 
 ![résultat](/uploads/ba544e818d39d9e569c0ce2d3f0df16b/résultat.PNG)
 
 Et pour avoir un bon résultat sans les virgules on est obligé de tout multiplier par 100 et tout diviser par 100 après
 
 ![bonne_opération](/uploads/62e5c9be7b1111ce59b7e06b3c5a0ee4/bonne_opération.PNG)
 
 ### Commande random:
 Qui veut dire aléatoire en anglais, s'utilise grace à une commande qui s'appelle ``Math.random`` qui nous permet d'avoir un nombre aléatoire.
 A vrai dire ce nombre n'est pas aléatoire car il basé sur l'horloge de l'ordinateur sur lequel on travaille.
 
 **Pour générer un chiffre au hasard:**
 
 ![random](/uploads/931f699b869692cad25339fd2a728a7f/random.PNG)
 
 ![random2](/uploads/bdedf2fb131b9393826fe9c5e9343edc/random2.PNG)
 
 ### Math.floor:
 Cette propriéte nous permet d'éviter les chiffres qui viennent après la virgule ``Math.floor`` exemple:
 
 ![MathFloor](/uploads/79358688022113884b95f7560caa9878/MathFloor.PNG)

![nomsPNG](/uploads/e09c06818bd80cc87e5ebf20e7745365/nomsPNG.PNG)