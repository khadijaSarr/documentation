# Array

## Rappels JavaScript:
En javaScript on a des normes universelles qu'il faut respecter, l'une des normes aujourd'hui c'est qu'on doit mettre que des simples cotes *````'...'````*,
autre norme c'est que les instructions finissent toujours par un point virgule *";"* et y a toujours un espace après une virgule.
quand on a un égal on doit mettre un espace avant et après 

 ### Console.log:
 permet d'afficher les instructions

## Qu'est ce qu'un array?
Un array qui veut dire tableau en français est un objet, une chose, un contenaire 
 dans lequel on va pouvoir mettre d'autres choses stocker des données (des nombres, des strings, des booléans, des prédicats, des objets, un autre tableau )
 

## Comment manipuler un array?
Tout d'abord on le déclare en créant une constante ``const`` ou une variable ``var``
qu'on va nommer et après il est suivi du'un égal ``=`` et des crochets``[]`` pour remplir des donnés. Dès que 
le tableau est déclaré on peut commencer à mettre des choses comme des nombres, des string... directement pendant que l'on déclare


## A Quoi sert un array?
 Il sert à manipuler les objets, les eléments qui qui s'y trouvent  grace à des opérateurs;
 par exemple on peut connaitre la longueur du tableau c'est à dire combien d'élements il y' a dans un tableau grace à la propriété ``length``
 et pour se faire on écrit ``console.log(tab.length); `` dans notre feuille javaScript et on éxécute dans notre console pour voir le résultat. 
 On peut aussi s'en servir pour faire des opérations des indexes
 Pour accéder à un élement d'un tableau c'est à dire l'afficher on utilise les indexes, parceque chaque élement d'un tableau a un indexe 
 qui veut dire son numéro dans la file. Le premier élément d'un tableau en JavaScript a l'indexe ``0`` c'est à dire qu'on commence à compter à partir de 
 zéro en informatique
 
 *Pour afficher l'élément du tableau on écrit:*
 ``console.log(tab[l'indice de l'élément];``
 
 *Pour afficher le dernier élément d'un tableau:*
 ``console.log(tab[tab.length -1]);``
 
 ![js_length](/uploads/e1ffa021ab48d36d3ea93873c0118889/js_length.PNG)
 
 ### Opérations des eléments d'un tableau:
 Pour faire des opérations dans un tableau on a besoin des opérateurs et on les met toujours à la fin d'un tableau
 par exemple pour additionner deux éléments on doit écrire:
 ``console.log(tab[n°indice] + tab[n°indice]);``
 
 **L'opérateur `ForEach`:**
 La méthode forEach() permet d'exécuter une fonction donnée sur chaque élément du tableau.
 
 ``le nom du tableau.forEach(element => console.log(element))``
 
 Exemple avec une fonction:
 
 ![forEach](/uploads/59c63698caabbfdb778675e480ef33ba/forEach.PNG)
 
 **l'opérateur `map`:**
 il parcourt le tableau et modifie ces éléments la façon dont fonctionne l'objet c'est un transformateur, il prend un petit bout de quelque et l'affiche,
 il fait comme une sorte de séléction

 ![map](/uploads/4a619c61bfc828b9509c3b52c98dbaf2/map.PNG)
 
 **l'opérateur `filter`:**
 Comme son nom l'indique permet de filtrer  
 
 ![filter](/uploads/f6b9b9f532c0c7e8575dcec704f197c0/filter.PNG)
 
 **l'opérateur `sort`:**
 Il permet de comparer les éléments il les prend 2 par 2
 
 ![sort](/uploads/37a461e4f3968d6541c8daa14fb5e85b/sort.PNG)
 
 Quand on doit utiliser ``sort`` dans des string on doit rajouter des fonctions ``if`` et ``return``
 pour retourner une valeur parceque sort ne peut pas comparer les string 
 
 ![sortstring](/uploads/ad2bc7cd246154e913618091c0ae55b8/sortstring.PNG) 
 
 *l'opérateur `every`:*
 Il permet de  vérifier que tous les éléments d'un tableau répondent à une condition (```prédicat```)
 
 ![every](/uploads/ac63b568d8091551989c9ff93c28499f/every.PNG)
 
  *l'opérateur `some`:*
  C'est l'inverse de ``every`` il permet de vérifier si un seul des éléments répondent à un tableau, donc
  s'il y a un seul élément du tableau qui répont à une condition il affiche ``true`` sinon il affiche ``false``
  
  ![some](/uploads/c921ef81a82e6753e64ffd6e607218c7/some.PNG) 
  
  *l'opérateur `push`:*
  La méthode push() ajoute un ou plusieurs éléments à la fin d'un tableau et retourne la nouvelle taille
  
  ![push](/uploads/d12ca0866e42e96a02b008612460b8bd/push.PNG)

 
 
 
 