# React:
Le react c'est du Javascript native avec un moteur enplus

``jsx`` javascript qui a des possibilités enplus que les autres  où on peut faire des return du DOM 
## Installation:
Il faut aller dans google et taper ``create react app`` et ensuite
cliquer sur le lien: ``facebook/create-react-app: Set up a modern web app ... - GitHub`` qui est crée par facebook 
mais on n'est pas obligé de choisir ce lien là parcequ'il y'en a d'autres c'est l'équivalent de l'angular CLI 

Après on copie ``npm i create-react-app -g`` qu'on colle sur notre terminal en se positionnant sur notre projet et on lance la commande

et ensuite on lance ``create-react-app `` suivi du nom de notre application sur laquelle on veut travailler et il va nous créer un projet
bien structuré.

### Rappels:
``-g`` ---> l'installation générale de l'application sur la machine 

La première chose à faire avant de commencer à travailler sur react c'est de supprimer les fichiers qu'on a pas besoin et après 
supprimer les imports qui des fichiers 

![CimportReact](/uploads/94e50e32bf62995fc2f1f34d51e90f6a/CimportReact.PNG)

: javascript native sauf qu'on ne peut pas return ``div`` directement sans les parenthèses 

![reactReturn](/uploads/f95e1e055833a807420b5304f07029bc/reactReturn.PNG)

En javascript native une **function** c'est la mème chose qu'une **class** sauf que pour des raisons de lecture et d'écriture c'est 
mieux de les présenter sous forme de class

### Comment marche un Browser (navigatuer)?
Il a comme point d'entrée le fichier HTML et c'est le plus important 

``DOM:`` c'est le Html et les méthodes liées aux balises; c'est l'outil qui permet l'affichage 

On a toujours la page de base (principal) qui est le `index.html` 
Un browser fonctionne avec un principe de `TOKEN`  
le browser va interpreter le fichier HTML sous forme de TOKEN et via les TOKEN il va interpreter la page et les differente chose a effectuer.
le TOKEN 1 a pour propriete de contenir la balise H1
le TOKEN 2 a pour propriete de contenir le texte "titre"
le TOKEN 3 a pour propriete de contenir la fin de la balise H1

En premier lieu le navigateur va lire la page HTML et tout ce qu'elle contient on fonctionant par TOKEN, et des que le Browser va tomber sur une balise `<script/>` elle va lire le JS ou le fichier ou est le JS en adaptant le JS sur les balise HTML en creer de nouveau TOKEN mis a jour avec le JS.


``virtual DOM:`` il copie le DOM et fait des modifictaions et une fois les modifictaions finies il les renvoit au DOM 

## bind:
