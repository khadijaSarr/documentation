# Firebase Functions
## TypeScript:
Le typeScript transpile en javaScript, ça transforme en langage A à langage B et c'est un syper set de javaScript c'est 
à dire que ça lui rajoute des pouvoirs. Pour utiliser typeScript on va prendre l'une des façon de faire qui va 
utiliser les functions de firebase qui permettent de faire du back office avec le server lands. Comme ça typeScript nous met en place tout le système.

### Configuration firebase functions:
La première chose à faire c'est de créer un nouveau dossier dans lequel on veut travailler en typeScript, après on ouvre notre terminal et 
on vérifie si on est bien dans notre dossier sinon on se met dans notre dossier.
Une fois qu'on est dans le dossier souhaité on tape ``firebsase init functions``, après on suit les instructions pour la 
configuration qu'on veut tout en choisissant notre projet de firebase sur lequel on veut travailler en se déplaçant avec 
**les flèches du haut et flèche du bat** et **Y** ou **N** pour valider son choix -----> ``typeScript`` -----> ``TSlint``
système pour avoir un code propre ------> ``install dependencies`` pour installer toutes les applications qu'on a besoin automatiquement.

![synchronize](/uploads/dce4f81d877cd283450d5385199aebf9/synchronize.PNG)

Une fois que c'est installé on clique droit sur notre dossier et on clique sur ``synchronize`` pour mettre à jour les changements 
et si on constate qu'il ya des fichiers rouges on clique droit sur notre dossier -----> ``Git`` -----> ``Add`` pour que
ça soit ajouter sur Git. Après on remarque qu'il a créé l'architecture du projet qui est dans ``functions``  et dans celui ci on a 3 fichiers
qui sont surtout à ne pas toucher. On a un dossier ``src`` pour coder en typeScript, ``package.json`` on peut en avoir plusieurs
dans une application, mais chacun s'applique au dossier concerné. Dans les packages.json on a des scripts qui sont pré-fabriqués
qui vont nous permettre de lancer des commandes automatiquement et pour les activer on clique droit sur ``package.json`` ----> 
``Show npm Scripts`` qui va nous permettre d'avoir une nouvelle fenètre qui est composée de:
````
build
serve
shell
start
deploy
logs
````

![npm_script](/uploads/baeeed7ce05d04e29df88c20b2a5b21e/npm_script.PNG)

et c'est la commande ``serve``  qui va nous interresser le plus pour l'instant parcequ'il va nous permettre d'exécuter le fichier ``index.ts``
qui nous évite de taper ``Node index.js`` qu'on faisait jusque là, d'ailleurs on aura plus besoin d'utiliser le terminal pour lancer
des programmes mais plutot ``les scripts npm``.

![npm](/uploads/4002bb6a1a20a8b017fc482395db85d9/npm.PNG)

### Coder en TypeScript:
Tout d'abord faire un ``console.log`` dans notre fichier ``index.ts`` pour vérifier que tout marche et on lance ``serve``. On constate qu'un port a été 
lancé pour nous donc plus besoin de faire ``app.listen suivi d'un port`` qu'on faisait avant on utilisera localhost suivi du numéro
de port qu'il nous a lancé dans notre navigateur pour voir le résultat.
En typeScript on utilise ``import`` au lieu de ``require`` et on remarque qu'on a notre code écrit en **typeScript** est transformé en code **JS**
dans le fichier **index.js** qui est dans le dossier **lib**

![import](/uploads/a07483737562d1cc06b3aecd8de67bcc/import.PNG)

#### Méthode pour relancer le serveur:
Pour éviter de stopper notre terminal et de le relancer après chaque modification du code on ajoute une nouvelle commande ``"watch": "tsc -w"``
dans notre fichier ``package.json``, on clique sur **Reload script** (actualiser) de la fenetre ``npm`` pour faire apparaitre **watch** et on clique 
sur ce dernier pour nous rafraichir les modifications directement et on aura plus besoin d'arreter le serveur tout le temps. Parcequ'on aura
serve et watch  qui tournent en parrallèle.

#### Express
Puisqu'on fait du express on va devoir installer ``express`` dans un premier temps et pour ce faire on va dans notre terminal on vérifie qu'on est bien 
dans le bon dossier et on écrit ``cd functions`` pour nous permettre de se déplacer dans le dossier functions ----> ``npm i -S express``.
En ouvrant le fichier **package.json** on doit avoir la dernière version **express** qui y est bien installée. Maintenant on doit importer express pour
pouvoir faire nos opérations tels que ``get, put, delete, apost`` et pour trouver la bonne façon d'importer on va sur ``google`` et on 
 ``typeScript import express`` et on copie ``import * as exress from "express";`` dans notre fichier index.ts
 
![import_express](/uploads/f03edde2efa2fc7e8f103759b9e9c34d/import_express.PNG)

![helloWordFunctions](/uploads/44cf3e8979819bc24efceb095124576b/helloWordFunctions.PNG)

#### Type et Models en typeScript:
Maintenant on peut commencer à coder proprement en typeScript de la méme façon qu'on faisait avant mais la seule différence c'est qu'ici on découpe
 les choses correctement pour nous éviter d'avoir un fichier immense. TypeScript comme son nom l'indique **type** veut dire qu'on va déclarer les types 
qu'on va utiliser pour nos data et ça change beaucoup de choses. Donc on est obligé de créer un nouveau dossier dans **src** qu'on va appeller 
**models** pour commencer à modéliser les objets qu'on utilisera dans l'application. Dans notre dossier qu'on a créé on va créer autant de fichiers ``.model.ts``
que de models qu'on a dans notre application. Pour faire un model on commence par la chose la plus profonde 

![roomModel](/uploads/a640a522984c6ed9b5258852db12908e/roomModel.PNG)

```
? devant une information : veut dire que les informations sont optionnels peuvent exister
```
![roomModel2](/uploads/dfbfc5e11fb0b1812a9445b877313fd8/roomModel2.PNG)

![hotelModel](/uploads/bf0a14b535f2c9fb092ccaacfdd4ddab/hotelModel.PNG)

```
alt + entrer: pour importer les fichiers à cotés automatiquement.
```
Aprés on crée un fichier ``database.data.ts`` à la racine du dossier ``src`` pour y mettre notre base de données mais on écrit ``export const`` avant

![arrayHotelModel](/uploads/9e9eed625134daf1244ee5970b6bbe88/arrayHotelModel.PNG)

On crée un nouveau fichier ``.ts`` dans **src** pour y mettre nos functions bien ``typées`` et rajouter ``export`` devant chaque funtion pour pouvoir les récupérer

```
alt + entrer + specify type explicitly: raccourci pour donner le type 
```
Une fonction doit toujours renvoyer le meme type de data il faut éviter de lui donner deux types de data 

![type](/uploads/f606de607d350aa0c4a78f5f4a230553/type.PNG)

Après avoir typer toutes nos fonctions on peut faire nos opérations ``app.`` dans notre fichier ``index.ts``

![appGet](/uploads/9b2909fbb680484849edb0ead556f9bc/appGet.PNG)

Et pour tester que tout marche on lance postman et on y met notre URL 