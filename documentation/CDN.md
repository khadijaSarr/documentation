# CDN

## Qu'est ce qu'un CDN?
c'est un réseau de machines qui mets à disposition de contenus 
![CDN](/uploads/e24fce49e71f22aaca00a06fe65045b2/CDN.PNG)

Il y a certains site *"assets"* qui sont dynamiques et y'en a d'autres qui sont statiques

### Assets Statique: 
On entend par page statique, non pas une page sans mouvements ou sans animations, mais une page visible telle que'elle a été conçue.
Ces pages peuvent présenter toute forme de contenu, animations flash, images, musique, vidéo etc... mais elles sont toujours présentées de la meme façon. Ells ne changent pas et c'est en ce sens qu'elles sont statiques.
 C'est une partie qu'on peut mettre du JS du HTML, du CSS
Pourquoi le JS parceque ça reste du texte mais c'est stocké statiquement
### Assets Dynamique:
c'est la base de données: Le JS ou le back office vont générés des Data qui vont etre injectés dans la partie statique

En opposition aux pages statiques, les pages dynamiques permettent de présenter les informations de différentes manières selon l'interaction avec le visiteur. 
Les pages sont alors construites "à la volée" grâce à une programmation conçue par le webmaster.
Le contenu est issu d'une base de données en fonction de critères établis par l'internaute puis mis en page en temps réel. 
C'est le cas par exemple d'un site e-commerce: présentation des articles par thèmes, couleurs, prix etc... 
C'est également le cas des blogs et des forums où les visiteurs peuvent participer au contenu du site. 
C'est aussi le cas d'un système de mises à jour.

La façon la plus intelligente de mettre à disposition des utilisateurs la partie statique ce n'est pas de la prendre sur un seul et unique serveur "ordinateur", mais plutot sur beaucoup d'ordinateur à travers le monde entier et ça c'est le CDN qui nous permet de le faire  

## Comment déployer un site sur un CDN
On va utiliser le meilleur CDN qui est celui de google et c'est Firebase.
### Firebase
La premiere chose à faire c'est creer un compte sur firebase et après on aura la console qui sera notre espace de travail principal sur lequel on pourra mettre notre projet
Donc on doit créer un projet on le nomme et ça nous crée un espace et des serveurs qui nous sont disponibles en fonction de l'endroit où nous nous situons.
 Firebase permet de gérer bcp de choses comme l'authentification, la base de donnés, du stokage d'images... Mais nous c'est hosting qui nous interresse c'est le fameux CDN en quelques sortes.
 
 ### Firebase Hosting
 Firebase Hosting est un hébergement de contenu Web de qualité production pour les développeurs.
   Avec une seule commande, vous pouvez déployer rapidement des applications Web et servir à la fois
    du contenu statique et dynamique vers un CDN (Content Delivery Network) global. 
    Vous pouvez également associer l'hébergement Firebase aux fonctions Cloud ou Cloud Run
     pour créer et héberger des microservices sur Firebase.
 Une fois le projet créé on clique sur Developper après Hosting et puis commencer, 
 on copie *"§ npm install -g firebase-tools"* on va dans notre Webstorm on ouvre notre console et puis on le colle 
 
 ![hosting](/uploads/3f23ea25ed95c7b2b93059c58399c335/hosting.png)
 
 
 ![hosting2](/uploads/db6a990c46ab166e006e06755904924b/hosting2.PNG)

 Maintenant on retourne sur Firebase Hosting on clique sur suivant et on copie les deux commandes qu'on colle dans notre console de commande
 
 *firebase login:* pour se loger "s'authentifier" donc forcément on doit créer un compte avant 
 
 *Firebase init hosting:* pour initier notre projet
 
 On éxécute oui et on choisit le projet qu'on vient de créer sur Firebase
 le dossier *Public*: on crée un dossier Public et on y déplace tous les dossiers qu'on avait créé et on vient chercher le fichier indexe et le deplacer à la racine du projet c'est à dire dans le dossier public directement.
  Alors là il nous demande quel dossier on veut rendre Public et on selectionne notre dossier qu'on a appelé Public.
  Après il nous demande si on veut écraser le dossier qu'on a crée on clique sur Non.
  Maintenant que l'initialisation est complète on tape *firebase deploy* et tous nos dossiers qui sont dans notre dossier "Public" sont envoyés sur internet.
  Donc on vient de créer une URL qui s'appelle comme le nom de notre projet quon a crée sur Firebase
 
 ![image](/uploads/4822f5ba6469e77caceb1a0de6e73c99/image.png)
 
  Après avoir mis notre site en ligne et qu'on remarque qu'on a pleins de fichiers et de dossiers rouges,
  on va aller ouvrir notre fichier gitignore et on y écris *".firebase"* et pas les autres qu'on a besoin
   
  
 
 
 
 
 
 
 
 
 
  
 
 
 
 
##