# Bootsrap
 
 
## Un navigateur
 C'est un "Parseur" terme provenant de l'anglais "parser" qui veut dire parcourir désignant un programme informatique d'analyse syntaxique. C'est quelque chose qui va parcourir le fichier HTML et en focntion de ce qu'il va lire, il va déclencher des actions 
  
 ## Installation de BootStrap dans un fichier Html :
   Copié l'ensemble des 2 link ( sur la page Bootstrap) Css et Js puis les introduire dans le head, juste avant le body
   
   
  ### :Lien CSS:
  
  ```` 
link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  ````
  ### Lien JS :
   ```` script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script
   
    script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script
   
   script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script
   
   Les parties allant de 'integrity' jusqu'a 'anonymous' ne sont pas nécessaire
   ````
 ### Pré-requis 
 
 Un div class="container" qui englobe l'ensemble du body
 Un élement Section pour chaque 'contenu' de la page, intégrant un header et un footer si nécessaire
 
 
 ### Les colonnes
 Chaque Section devra être (du moins pour débutants) englobé par par les div suivants :
 
 div class="row"
 div class=col-(un chiffre)
 
## Architecture pour faire un site internet statique
 Quand on veut faire un site internet la première chose à se poser c'est combien de page on veut avoir. En fonction du nombre de page on va créer autant de dossier.
 Et pour un site internet classique on crée 4 dossiers: 
 
 - "Home page": Landing page qui veut dire attérissage
 
 - "About": A propos de nous
 
 - "Faq"
 sous forme de 3 directory: dossiers
  
 Ensuite on va créer une page qu'on va appeler "Assets" dans assets on peut mettre un dossier "img" et un dossier "vendors"
 le dossier "img" pour stocker nos images et le dossier "vendors" on peut s'en servir pour stocker tous les scripts ou css ou HTML qu'on importe qui viennent d'une tierce personne  
 
 ##  3balises à connaitre pour le référencement:
 
 ![Capture0](/uploads/cf9de1c2e6d94718023f9ca530014429/Capture0.PNG)
 
 ### Et donc pour 3 parties on aura
 
 ![Capture](/uploads/4f80b886037679a40b1e59d7059a0f37/Capture.PNG)
 
 Google comprendra alors qu'il s'agit de 3 parties centrales.
 
 
 #### Important: un h1 par page.
 
 Ne mettez pas de footer si vous n'avez rien à mettre dedans et titrez(h1, h2...h6).
 Commencez toujours par le coeur des informations avec la structure, le css venant après.
 
 ## Utilisation de Bootstrap
 
 Copier les links dont vous avez besoin.
 Par exemple, "card title", "button", "badge" "breadcrumb" etc...
 Cross-browser : Il s'adapte parfaitement aux différents navigateurs, ces derniers réagissant tous de la même façon
 
 Responsive : L'une des particularité principales de Bootsrap, Avec une base de 12 colonnes permettant une adaption à chaque type d'écran
 
 Normalisation : Via l'emploi de nombreux outils complémentaires : Normalize/LESS (voir fichier dédié)
 
 ![Capture1](/uploads/d9e4f932dfdbb29d1304e624189d3dd0/Capture1.PNG)
 
 le bouton bleu est appelé **"call to action (CTO)"**
 
 ![Capture2](/uploads/5ae03f7cb1cefdc84ecb1d997cb86da7/Capture2.PNG)
 
 ## 2 balises à connaître:
 
 div en diplay block( définir des blocks)
 span en display inline( des mots collés les uns aux autres)
 Attention à l'emplacement de vos fichiers(par exemple les IMG)
 
 ![Capture3](/uploads/6e4faaf4fa9e17c70278fbe3dd8af1a8/Capture3.PNG)
 
 ## CONTAINER, ROW ET COL:
 
   Un site qui utilise bootstrap doit être dans un container(sorte de div principal dans laquelle bootstrap vous oblige à mettre son contenu).
    Par conséquent il faudra créer une classe container.
    Le container permet une meilleure responsivité du site et container-fluid signifie que le contenu prendra la plus grande place possible.
    
   ![Capture4](/uploads/0508ceaafebd188bbf457570957391ab/Capture4.PNG)
    
   Il faut organiser le site en fonction de lignes et de colonnes. Pour ce faire, on utilise les "row et col" en créant des "class".
    Un row est toujours accompagné d'un col.Sachant que la page est divisée en 12 col. Si l'on met simplement "col", cela contiendra les 12 col.
    
   ![Capture5](/uploads/b3fb27b313fb7f9b60cf345fcd96bcf0/Capture5.PNG)