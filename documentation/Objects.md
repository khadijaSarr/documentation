# Transformer un objet en tableau

Dans un objet on a des clés ``key`` qui sont des attributs et des valeurs ``values`` qu'on les attribue. 
On ne peut pas transformer tout l'objet en tableau, on récupère soit que ces **clés** soit que ces **valeurs**.

**Rappels**: l'opérateur ``Delete`` permet de supprimer l'élément d'un objet quand on l'a en double.

![objets](/uploads/3933e4a71b14a812c2ccc0229fc07197/objets.PNG)

Pour changer les clés et valeurs d'un objet il faut forcément transformer l'objet en tableau

## Transformation des clés d'un objet en tableau:

![clé_tableau](/uploads/313c070b4d68a2f6d13aea6d50e15aaa/clé_tableau.png)

### Modifier les clés d'un tableau:

![keys](/uploads/02521415d25b21cf98d5d2fa33ce82a7/keys.PNG)

![chander_les_clés](/uploads/4d27d8b24ef8de2f77d146c6b1fb6acb/chander_les_clés.PNG)

## Transformation des valeurs d'un objet en tableau:

![tableauValues](/uploads/5b45f2a3ed942d7b855153772cb6bff6/tableauValues.PNG)

On a 2 façons d'acceder aux propriétés d'un objet, on utilise des crochets quand on ne connait pas le nom de la clé qu'on veut chercher
 c'est à dire qu'il nous vient d'une variable 

![meme_chose](/uploads/0d5fd8d8a9a5183ec44f8e4a3eaa5efb/meme_chose.PNG)

### Modifier les valeurs d'un tableau:

![valeur_tableau](/uploads/2e0fd009c62ead79385380d771342380/valeur_tableau.PNG)

