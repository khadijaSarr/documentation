# Gitignore

## les fichiers cachés:
Il y a un certain nombre de fichiers cachés qui contiennent des informations sur Git en les supprimant ça supprime tous les fichiers dans Git.
Normalement quand on commence à faire un projet il faut d'abord créer un fichier **".gitignore"** ça permet de ne pas poluer notre ripogit avec des choses qu'on a pas besoin  

## Comment parametrer le navigateur pour gerer les fichiers cachés:
 
 il faut creer un fichier qui s'appelle **"gitignore"** en mettant un **"."** d'abord après gitignore et on ouvre le fichier et on mets **"."** 
 et après ça il nous propose des fihiers qui commencent par un point et on selectionne celui qu'on veut
 
 ![creer](/uploads/2be679f5c70bb70d7b6a71d4e776ab75/creer.PNG)
 
 ![gitignore](/uploads/d181aedd1b16d48db76e80e294560688/gitignore.PNG)

### .idea:
C'est un dossier qui contient tous les fichiers de configuration de notre naviagateur **webstorm** 
le point devant veut dire que c'est un fichier caché donc n'apparaitra pas et le fichier s'appellera **"."** suivi du nom qu'on l'a donné.
Pour voir les fichiers cachés sur Webstorm on click sur Project et on click sur Project Files et les fichiers cachés sont en couleur bizzare pour dire qu'ils seront pas pris en compte par Git
et c'est une vue qu'on ne doit pas utiliser ça nous permet de savoir ce que connait **GIT** pour pouvoir l'ajouter au **gitignore** en cas de besoin
 
 
![prpject](/uploads/d6dca7d04a4775fec10802a191edc834/prpject.PNG)