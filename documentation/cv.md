# Technicien Supérieur en Informatique

**Khadijetou Sarr**  
0614330291  
14 Rue Fortune Charlot  
Chez CCAS 95370 Montigny Les Cormeilles  
_khadijasarr8@gmail.com_

![tof_-_Copie_-_Copie](/uploads/4f9e8c9bb086aee23a6d3a62f79c9757/tof_-_Copie_-_Copie.jpg) 

#### Rigueur,organisation,Sens de l'écoute et de la communication, adaptation et travail en équipe

## Compétences   
- Changer ou réparer un élémént ou un ensemble défectueux  
- Identifier des ressources nécessaires à la résolution d'un dysfonctionnement   
- Monter, installer et mettre en service les nouveaux matériels informatiques 
- Langages de programmation informatique
- Supérviser et vérifier l'état des ressources informatiques, réaliser les sauvegardes et les archivages
- Système d'exploitation informatique Android

   ***
## Expériences Professionnelles
2012/2017  **Technicienne Supérieure en Informatique** - Agence Nationale du Registre des Populations et des Titres Sécurisés (cac du Ksar) - Nouakchott - Mauritanie  
2011/2011  **Enquetrice MICS** - Office Nationale de la Statistique en collaboration avec l'UNICEF et FNUAP - Nouakchott, Mauritanie  
2011       **Stage Pratique portant sur la création des sites web et la maintenance du matériel informatique** - Maurinet - Nouakchott, Mauritanie
2010       **Projet Informatique sur le fonctionnement du marché monétaire primaire et secondaire** - BCM, Mauritanie   
2007       **Marketing et Ventes** - TIGO, Sénégal
   ***
## Formation
2009/2010  **Licence Pro Génie Informatique (Génie Logiciel et Bases de Données)** - Université Gaston Berger de Saint Louis - Sénégal
2007/2009  **Diplome Universitaire en Réseaux et Informatique de Gestion** - Université Gaston Berger de Saint louis - Sénégal

   ***
##  Loisirs
Marche, Cuisine, Musique 

