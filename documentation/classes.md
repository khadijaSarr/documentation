# Les classes:
La première chose à savoir sur les classes c'est que c'est un outil qui a été développé après le démarrage en informatique pour aider les développeurs à
 conceptualiser des choses qui étaient assez compliquées à faire en terme de code. 

Le système de classe est très utile à partir du moment où on utilise son outil principal qui est l'héritage. Une classe ça se déclare avec le mot clé 
``class``. Une classe c'est aussi un peu comme un objet donc on va y avoir des propriétés mais elle a quelque chose de plus qu'un objet en interface 
c'est qu'elle a un ``constructor``.

 ![classe](/uploads/d4565f3e32f364f39687db3709cc5680/classe.PNG)
 
Un constructor c'est une fonction dont le nom est imposée, elle est destinée à créer une instance d'une classe. Avoir une classe c'est bien mais ce qui est
 important c'est d'avoir une instance de la classe pour pouvoir manipuler la classe.
 
  Pour créer une instance on utilise le mot clé ``new``. Quand on doit appeler un constructor pour qu'il fonctionne on doit mettre les mots clés dedans
   comme id par exemple. Ca nous permet de récupérer le mot clé dans le constructor et de le coller à l'intérieur de l'instance et pour ce faire on utilise 
   le mot clé ``this``

![constructor2](/uploads/a82784dc7887405632baefcf0b2c7420/constructor2.PNG)

![constructor](/uploads/1732d29aa603e25bcefa6e405cc36d8d/constructor.PNG)

Si on a des données qui ne sont pas très obligatoires on peut mettre un point d'interrogation en les déclarant, comme pour les interfaces les points d'interrogations
doivent toujours venir à la fin comme là par exemple:

 ![données_optionnelles](/uploads/4c83fbca7a1c09f34f654ad46e8363b1/données_optionnelles.PNG)

```
import {RoomModel} from "./room.model";

export interface HotelModel {
    id?: number;
    name?: string;
    roomNumbers?: number;
    pool?: boolean;
    rooms?: RoomModel[];
    isValidated?: boolean;
}


export class HotelClass {
    id?: number;
    name?: string;
    roomNumbers?: number;
    pool?: boolean;
    isValidated?: boolean;

    constructor (
        id: number,
        name: string,
        roomNumbers?: number,
        pool?: boolean,
        isValidated?: boolean,
    ) {
        this.id = id;
        this.name = name;
        this.roomNumbers = roomNumbers;
        this.pool = pool;
        this.isValidated = isValidated;
    }
}
```

Quand on crée une classe normalement il ya une règle en Js qui dit pas plus de 4 arguments à une function au lieu de répeter les ``this.object = object`` 
on utilise ``Obiects.assign`` c'est à dire qu'on va prendre les propriétes d'un objet **A** qu'on va inserer dans un objet **B** comme suit:
 
 ```
export class HotelClass {
    id?: number;
    name?: string;
    roomNumbers?: number;
    pool?: boolean;
    isValidated?: boolean;
  
    constructor (hotel: HotelModel) {
       Object.assign(this, hotel)
    }
```

Il faudra juste modifier les paramètres comme suit:

```
app.get('/', async function (req, res) {
const myHotel = new HotelClass( {id: 12, name: 'hotel des fleurs'});
console.log(myHotel.id);
res.send(myHotel);

});

```
L'autre avantage d'une classe c'est quand on la définit on peut la donner des méthodes c'est à dire des functions spéciales qui vont s'appliquer aux 
instances qui vont pouvoir manipuler les objects par exemple:

```
  calculateRoomNumbers() {
      this.roomNumbers = this.rooms.length;
    }

```
Donc il faudra juste appeler la function à chaque fois qu'on en a besoin puisqu'elle est déjà faite comme ici par exemple:

```
app.get('/', async function (req, res) {
const myHotel = new HotelClass( {id: 12, name: 'hotel des fleurs'});
myHotel.calculateRoomNumbers();
res.send(myHotel);

```
 
A partir du moment que nos classes renvoient ``this`` on pourra éviter d'appeler plusieurs fois notre ``const`` c'est à dire l'appeler une fois et
mettre les opérations qu'on veut appeler à la suite comme suit:


`````
app.get('/', async function (req, res) {
const myHotel = new HotelClass( {id: 12, name: 'hotel des fleurs'});
myHotel
    .calculateRoomNumbers();
    .createPool();
res.send(myHotel);

`````

```
  calculateRoomNumbers() : HotelClass {
      this.roomNumbers = this.rooms.length;
      return this;
    }

  createPool() : HotelClass {
      this.pool = true;
      return this;
    }

```
 Le mot-clé ``extends`` est utilisé dans les déclarations et expressions de classes afin de signifier qu'un type représenté par une classe hérite 
 d'un autre type.
 Le mot-clé ``super`` est utilisé afin d'appeler ou d'accéder à des fonctions définies sur l'objet parent.
 
```
export interface LuxHotelModel extends HotelModel {
     numberOfStars: number;
     numberOfRoofTops: number;
 }
 
 export class LuxHotelClass extends HotelClass {
     numberOfStars: number;
     numberOfRoofTops: number;
     
     
 constructor (hotel: LuxHotelModel) {
       super(hotel);
        this.numberOfStars = hotel.numberOfStars || 1;
        this.numberOfRoofTops = hotel.numberOfRoofTops || 1;
    }
}
```

Le mot-clé ``implements`` ça permet de s'assurer qu'on est le meme modèle

![implements](/uploads/4e28b8713543d3923b5de0dd243005db/implements.PNG) 

Pour générer un constructor on clique droit sur **la classe ---> Generate ---> Constructor ---> Shift fin ---> ok**