# Reduce

## Rappels:
En javaScript on a 3 types d'opérateurs 

### 1er type d'opérateur:
Ce sont des opérateurs qui parcourent un tableau mais ne fabriquent rien d'autres de spécial si ce n'est que de le parcourir

### 2eme type d'opérateur:
Ces opérateurs parcourent un tableau et fabriquent un autre tableau en sortie

### 3eme type d'opérateurs:
Ces opérateurs fabriquent autre chose qu'un tableau mais on peut pas le savoir à l'avance tant cela va dépendre du paramétrage et de l'opérateur comme ```every``` par exemple
 
 ## l'opérateur Reduce:
 c'est un opérateur qui renvoit quelque chose mais mais que nous allions décider à l'avance 