# Cours de Markdown

## Cest quoi le Markdown 
Le Markdown est un langage qui nous permet de prendre note et de faire des mises en forme.
Il est super important dans notre vie de developpeur parce qu'on s'en servira en permanence pour faire notre popre
 documentation. Le markdown est très pratique parceque ça nous donne toute suite quelque chose de propre de claire à lire. 
 On peut s'en servir aussi dans Slack On peut y mettre aussi des images comme suit:
Pour créer un titre, ajoutez des signes numériques (#) devant un mot ou une phrase. 
Le nombre de signes numériques que vous utilisez doit correspondre au niveau de l'en-tête.
Par exemple, pour créer un niveau d'en-tête trois (`<h3>`), utilisez trois signes numériques (p. ex. #### Mon en-tête).


### Comment placer une image sur Marckdown: 
Pour mettre une image sur Marckdown il faut aller ouvrir Gitlab ouvrir notre projet,
cliquer sur le bouton ***" + "*** et on choisit new **"snippet"** aller à l'image le glisser dans la partie à remplir 
et ensuite on selectionne le lien qu'on colle sur notre webstorm à l'ndroit où on veut et si la méthode **"+"** ne marche pas 
on utilise **"issues"** après **"new issues"** qui est sur Gitlab

#### Exemple D'image:

![Capture_marckdown](/uploads/182226b00c44760fa5dcc33d1d405df6/Capture_marckdown.PNG)

![Capture2](/uploads/c0ae2bdd7183877fec4bc9621a747cb4/Capture2.PNG)

![Capture3](/uploads/8e881e4ee680d4ba51e489a638cca356/Capture3.PNG)

![Captur4](/uploads/6c727d558e96996c32702be5b59838a7/Captur4.PNG)

![Capture5](/uploads/7301445d380c2db3ab0d613ac3d31425/Capture5.PNG)

![Capture6](/uploads/921b1b9e421fc6e56e5a8c16db5d3f06/Capture6.PNG)

![Capture7](/uploads/1bc8023d1cf24aaf0b7807e859afc95d/Capture7.PNG)

![Capture8](/uploads/40d364f4bd3ec4b6e8e57d661435a558/Capture8.PNG)

![Capture9](/uploads/255bc5d662f0ebc2c8693ea89b70dd7e/Capture9.PNG)

![Capture10](/uploads/62fff6d6b06f153ef54823f986f3e6a5/Capture10.PNG)

## Gitlab
C'est un site internet qui utilise Git

### Git
C'est un protocole qui permet de stocker du contenu informatique ou autres tel que les Github guitlab

### Comment stocker mon Markdown avec Gitlab
Pour ça on crée un nouveau projet sur Guitlab en cliquant sur _"New Project"_ qu'on renomme et après on click sur
 _"Public"_ pour etre vu par tous puis on coche sur _"Initialize with a ReadMe"_
Maintenant pour écrire dans ce projet grace à Webstorm il va falloir créer un lien entre webstorm qui est sur notre 
ordinateur pour que si on crée quelque chose sur webstorm ça se modifie directement sur le site internet.  
Donc pour ça on a un bouton à droite qui s'appelle _"clown"_ donc on l'ouvre et on copie l'URL http, une fois copié on 
 on ouvre Webstorm qu'on avait fermé au préalable et sur l'ecran de démarrage par défaut on clique sur _"checkout from version control"_
 et on choisit _"Git"_ et on colle ce qu'on avait copié sur Clown, on clique sur _"test"_ il nous demandera de nous identifier, 
 après l'identification il nous proposera de d'ouvrir un projet on clique sur _"Yes"_ et on aura un dosssier sur lequel il ya un fichier _"Readme.md"_
 et ensuite on peut commencer à créer sur autant de sujet qu'on veut.



## Les Backticks

Les Backticks permet de mettre du texte en surbrillance ou bien d'ecrire des
extraits de code tel qu'il serait en vrai.

### Backtick simple

Pour l'utiliser on met 3 backticks avant le code en question et 3 a la fin.
Apres les 3 backticks on choisit le type de code (JS, C , etc...).

### Backtick triple

Pour l'utiliser on met 3 backticks avant le code en question et 3 a la fin.
Apres les 3 backticks on choisit le type de code (JS, C , etc...).

![Capture_backtick](/uploads/c014bf937f91544d48b89035074145df/Capture_backtick.PNG)

## Comment structurer ma page
La structure d'une page doit etre organiser en plusieurs parties elle meme avec des sous parties.

Une page internet doit comporter uniquement un seul H1 le titre le plus important de la page qui va porter sur le sujet de la page.
Les balise H2 H3 H4 ont une ordre d'importance qu'il faut respecter.
Organiser les differentes partie avec leurs contenue et leurs sujets (comme une dissertation).
L'organisation est super importante car elle permet un meilleur SEO (Search Engine Optimization), meilleur référencement sur internet.


## Comment sauvegarder Mon cours sur Gitlab
Aller sur le site Gitlab.com, se connecter et recupérer sont lien HTTPS GitLab.


### Envoyer sur GitLab
Pour envoyer un fichier ou dossier sur GitLab on effectue un commit, le commit permet de choisir les fichiers, dossiers et autres a selectionner pour l'envoie

![Capture](/uploads/b604242f7bf2e5a4c4ca2c24dc9610a1/Capture.PNG)

![Capture2](/uploads/6f088de4903e3d8ba3046f2e3462fbe7/Capture2.PNG)

![Capture3](/uploads/0daee32577f23a310eebabf32fc335ad/Capture3.PNG)

![Capture4](/uploads/c29bdeff08d08cee833606fcdd1c5253/Capture4.PNG)

On peux voir le nom du commit que l'on a mis et ce que l'on veux push