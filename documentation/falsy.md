# Falsy
Ce sont des valeurs qui sont équivalentes à **false** donc forcément dans le premier ```falsy``` on a ``false``. Mais on a aussi d'autres valeurs en 
javaScript qui sont des falsy mais qu'on s'y attend pas forcément donc on doit connaitre nos ```falsy``` pour éviter des problèmes.
Il y a une liste de **falsy**, si on n'est pas dans cette liste c'est un **truesy** qu'on a forcément: 


## liste des Falsy:
**false**, **0**, string vide **''** qui n'est pas à confondre avec un string avec un espace sinon ce n'est plus un string vide, 
**null**, **undefined**, **Nan**.

**Attention** Tableau vide ```[]``` et objet vide ``{}`` ne sont pas des **Falsy**
 
 ### Exemple de Falsy:
 
 ![falsy](/uploads/99c474d27032d92cadfbe3e24acc6d47/falsy.PNG)
 
 ![exempleFalsy](/uploads/f6040dd4c686c5335f6b2f5bda358456/exempleFalsy.PNG)
 
 ![undefined](/uploads/d70079469ba5826594d5fc8dfdb28ae3/undefined.PNG)