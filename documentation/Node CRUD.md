# Node:
Node est une interface dont héritent différents types d'objets API DOM. 
Cela permet à ces types d'être traités de la même façon; par exemple, 
héritant du même ensemble de méthodes ou testé de la même manière.
Tout d'abord on fait du node en **server less**qui veut dire sans server, 
mais avant tout on commence par faire le node avec server qui va tourner sur notre ordinateur.

## Comment activer Node:
On doit télécharger la verion qu'on veut sur le site officiel du node en tapant ``node.js``.
Après pour vérifier que Node marche on va dans notre terminal sur Webstorm, on tape ``node --version`` 
la commande nous renvoit la version qui est installée sur notre ordinateur. Il est important de s'assurer que dans notre 
console on est bien à la racine de notre projet avant d'éxécuter les commandes.
En node on a un système qui permet de télécharger des minis applications, ces outils nous 
permettent de travailler en temps que Developpeur et pour mettre en marche la possibilité de lancer ces applications, 
on utilise ``Npm``.   

![nodeversion](/uploads/6e411218c63df8fb2a3b972caa1b9c03/nodeversion.PNG)

### Npm:
C'est un gestionnaire de paquets on peut dire l'équivalent de play Store en node. Pour l'initialiser dans notre projet on 
écrit ``npm init`` dans notre terminal. Pour savoir si npm est bien installé on vérifie s'il ya un fichier **package.json**
dans notre projet. Dans Package .json on a 2 types d'informations: Les ``devDependencies`` et les ``dependencies``
 
#### Npm init:
Quand on commence un nouveau projet et que y'a pas de ``dependencies`` installés il faut écrire ``mpn init`` 
dans notre console pour que les fichiers ``packages.json`` soient installés et après on écrit  ``npm i -S express``
si on veut les stocker dans les fichiers Dependencies ou ``npm i - D express`` si on les veut dans les devDependencies  

![Npm](/uploads/dbcac02d865abf58ac9b0cbe51f2a407/Npm.PNG)

#### DevDependencies:
Ce sont les applications qu'on va avoir besoin pour developper 

![devDependencies](/uploads/a2e8d373fd9610b09e43a50ccd042580/devDependencies.PNG)

#### __Dependencies__:
Ce sont les applications qui vont etre utilisées dans notre application finale par le client

![dependencies](/uploads/2c301365e7136657d97ecedcb96ee7c0/dependencies.PNG)

Aprés avoir installer Npm on va dans clique sur **file**, **settings**, on tape node dans notre barre des taches 
et sur **Node js and NPM** on coche sur **Coding assistance for Node.js** et enfin **OK** pour pouvoir faire du node correctement. 
Pour créer un nouveau fichier dans Node on se met sur notre projet et on clique droit après **New** => **File**
on peut le nommer **indexe.js** pour commencer. Quand on veut exécuter notre code JS on tape **node index.js**
dans notre terminal et entrer

### Express:
Express est une infrastructure d'applications Web Node.js minimaliste et flexible qui fournit un ensemble de fonctionnalités
 robuste pour les applications Web et mobiles. pour le faire marcher on va dans le site ``Express`` et on a toutes les informations pour 
 pouvoir l'utiliser. On choisit un port pour pouvoir écouter, parcequ'il ya différents ports dans notre ordinateur,
 en gros ce sont les endroits disponibles qui permettent à notre ordinateur d'etre accessible partout par internet.
 Après avoir choisit notre port, on vient sur notre terminal, pour lancer notre fichier ``index.js``
 , le terminal nous confirme qu'il écoute sur le port qu'on a choisit et après on va dans le navigateur et on tape 
 ``localhost`` suivi du numéro de notre port pour nous aider à écouter les servers qui sont sur notre ordinateur.
A chaque fois qu'on modifie quelque chose sur notre code JS on doit redemarrer notre server dans le terminal et pour se faire on tape 
``ctrl + C`` pour éteindre notre server et après on éxécute ``Node index.Js``   
 
 ![port](/uploads/b00e58ceecb908488d632b761d6cdf0e/port.PNG)
 
 ![express](/uploads/0b60643c07f50e55559f66200012b5eb/express.PNG)
 
 ![localhost](/uploads/2b1d334ff67e3c99dd9c5f136c22bfeb/localhost.PNG)

## CRUD
qui veut dire **Create**, **Read**, **Update**, **Delete**, si jamais on nous demander de faire un CRUD sur quelque chose 
ça signifie de faire toutes ces opérations. C'est très important parceque la plupart du temps en informatique
 on sera amener à faire du CRUD  
   
### Create:
C'est quand on veut créer une ressource 
```
app.post('/hostels/:idHostel', function (req,res){
    const hostelCreated = parseInt(req.params.idHostel);
    const createRoom = req.body;
    const roomCreated = addRoom(createRoom,hostelCreated);
    res.status(201).send(roomCreated);
```
### Read:
C'est pour lire quelque chose, guetter

```
app.get('/hostels/:id', function (req,res) {
    const id =  parseInt(req.params.id);
    console.log(id);
    res.send(getHotelById(id));
});
```

![get](/uploads/829404339f990f086095541fe255938d/get.PNG)

### Update:
C'est pour mettre à jour
```
app.put('/hostels/:id',function (req,res) {
    const createdHostel = req.body;
    const idHostel = parseInt(req.params.id);
    const hostelCreate = putHostel(createdHostel,idHostel);
    res.send(hostelCreate);
});
```

### Delete:
C'est pour supprimer 

```
app.delete('/hostels/:id',function (req,res) {
    const id = parseInt(req.params.id)
    console.log(id);
    res.send(removeHostel(id));
```

![delete](/uploads/6eac7232e4077eb76e17cedda62a3f51/delete.PNG)

**Req**: il nous permet de récupérer la requete qui est faite par l'utilisateur

**Res**: c'est l'outil qu'on va utiliser pour répondre à l'utilisateur, c'est pour renvoyer un résultat  