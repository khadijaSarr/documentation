# Function

## Rappels:
Pour annuler un fichier sur webstorm, on utilise le bouon à droite avec la petite flèche de retour,
quand on clique dessus il nous demande le fichier qu'on veut ``revert`` et on choisit celui qu'on veut annuler.

![revert](/uploads/8d3cc4436671fd9fc3dacfdcb744d133/revert.png)

## C'est quoi une founction:
Une function c'est quelque chose qui déclenche des ordres et ces ordres sont écris dans 
la zone qui est entre accolades ``{}``
Il faut se débrouiller pour qu'une function ait toujours une valeur en retour pour ne pas 
 se retrouver avec des ``Undifined`` d'où vient le role de ``return `` qu'il faut toujours 
 mettre dans une function pour avoir une valeur de retour.
 Par exemple si on ne veut rien retourner en JS on met Return ``void`` qui veut dire vide, néant.
 Retenons qu'il ya des fonctions qui retournent une valeur comme il y a des functions qui ne retournent rien
 

## comment la déclarer:
 Le mot clé pour déclarer une function est ``function``, après avoir donné un nom 
 à notre fonction on met directement des parenthèses ``()`` qui vont servir à mettre des 
 paramètres et ensuite on met des accolades ``{}`` dans lesquelles la fonction va s'exécuter.
 Quand on appelle une function Les parenthèses sont obligatoires et quand on tombe sur un 
 ``return`` la function arrete de s'éxécuter c'est à dire qu' il met fin à la function 
 
 ### Exemple de déclarartion de function:
 
 ![function](/uploads/adc6e8d97166a5d69d36742cddf5fa7c/function.PNG)
 
 ![function_produit](/uploads/9cca0c9c00de6c3a5d726d2d279aa7fc/function_produit.PNG)
 
 ![return](/uploads/a31a5581000dfba71074479ee3d3800a/return.PNG)
 
 ### Rest Opérator:
 Dans une function on peut mettre autant d'arguments qu'on veut dans la parenthèse, par contre il 
 y a une norme qui ne doit pas dépasser 4 parceque sinon ça devient ingérable.
 Par contre si on ne sait pas combien de paramètres on veut déclarer il y a un opérateur qui nous 
 permet de le gérer. Il s'appelle ``rest opérator `` ``...``
 Il nous permet de prendre tous les arguments autant qu'il y'en a et le ranger dans un array donc 
 il renvoie un tableau.
 On doit avoir qu'un seul rest opérator et doit toujours etre en dernière position  
  
  ![rest_opérator](/uploads/6d52ca9a07b9a79a82be136647163395/rest_opérator.PNG)
  
   ![rest_function](/uploads/8f051e96cf390274205c3c906f9975ee/rest_function.PNG)
   
 ### Spred Opérator:
 Il sert à créer de objets et les fusionnent 
 il s'écrit comme le rest opérator ``...`` mais contrairement lui on peut l'utiliser plusieurs fois 
  