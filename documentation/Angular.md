# Angular:
C'est un framework du javaScript qui a 2 types d'outils: modules et components, il nous permet de découper les choses en modules 

**modules:** C'est quand on regroupe tous les contenus qui parlent d'un meme theme ensemble

**components:** Dans un module on aurait 2 types de components, l'avantage d'un component c'est qu'il charge ses propres données tout seul, 
il s'autho-initialise tout seul et on peut le mettre n'importe où et n'importe comment. Pour les faire fonctionner on a la possibilité de leur faire 
passer des entrées et des sorties   

 
## Comment installer angular?
La première chose à faire pour que Angular marche c'est qu'il faut installer ``angular CLI`` et pour ce faire on va regarder la documentation officielle.
C'est à dire qu'il faut aller sur google et taper **Angular CLI**. 

**CLI: veut dire Command Line Interface** c'est un outil d’interface de ligne de commande que vous utilisez pour initialiser, développer, échafauder
 et gérer des applications angulaires. Vous pouvez utiliser l'outil directement dans un interpréteur de commande ou indirectement via une interface
  utilisateur interactive telle que la console angulaire .
  
Une fois dans la doc on copie ``npm install -g @angular/cli`` qu'on colle dans notre terminal et on tape sur entrer et après on tape **yes**.

Une fois la CLI installée on crée un dossier et on se positionne sur notre dossier dans notre terminal en se servant de ``cd ..`` pour se déplacer 
et on peut lancer ``ng --version`` pour vérifier que Angular est bien installée et après on y lance ``ng new myAngular`` pour qu'il nous crée un 
nouveau dossier ``myAngular`` dans le dossier qu'on a créé et après on ajoute le Angular routing en tapant sur yes et enfin on choisit le type de 
``CSS`` avec lequel on veut travailler et on sélectionne ``SCSS``. Une fois l'installation finie on peut ajouter notre dossier dans **Git** et cliquer 
droit sur **synchronize** pour mettre à jour sur webstorm. 

```
e2e: test unitaire, c'est quand on veut tester q'une application marche automatiquement
tsconfig: le fichier qui sert à configurer typeScript 
<app-root>: c'est une balise qui n'existe qu'en angular 
 AppModule: module principal qui va démarrer l'application 
environnements: des fichiers dans les quelles on va stocker les informations 
assets: c'est pour mettre nos images, nos polices de caractères... 
```

En angular pour définir les modules on se sert de **``@NgModule``** et on peut créer nos propres modules. Un component est composé d'un template c'est à 
dire le html, d'une feuille de style et du javaScript donc quand on crée un component automatiquement il vient avec ces 3 choses   
```{{}}: c'est pour dire en angular qu'on doit récupérer une des propriétés qu'il ya dans une classe  ```

```double biding: c'est quand on modifie le html et que ça se modifie automatiquement dans le component``` et pour ça on utilise 
``<input type="text" [(ngModel)]="">`` 

![ngModel](/uploads/d796bcfce9d24ea0f23de6d8ae5dc631/ngModel.PNG)

![ngModel2](/uploads/7a2052cf743a6194866fbe554d476bac/ngModel2.PNG)

## Comment faire parler angular avec node? 
On va utiliser angular pour pouvoir parler à des applications externes quand on voudra faire une API. Quand on fait du CRUD en angular il y'a une 
règle d'or importante à retenir c'est qu'on appelle pas une base de données dans un component.
 
 On se sert d'un outil qui s'appelle ``service`` ce dernier est comme les modules et les components , cest une classe qui a un décorateur spécial
  qui lui donne un super pouvoir du singleton
  
``Singleton:`` C'est une classe qui n'a qu'une seule et unique instance et qui nous renvoie l'instance qui a été créé avant à chaque fois qu'on essaye 
 de refaire un **new** sur cette classe.
 
``les services:`` ce sont des singletons qui n'existent que dans le cadre de leur module 

Pour créer un premier service on tape ``ng g s hostels`` **hostels** ici représente le nom du service, dans notre terminal surtout en se positionnant 
dans notre dossier angular.

Pour faire des appels au back office on va avoir besoin de faire des appels http et comme en angular quand on veut se servir d'un outil il faudra le déclarer 
dans **le constructor** donc on va mettre le mot clé ``private`` pour dire qu'on s'en servira que dans le constructor de notre service 

![privateconstructor](/uploads/39cf2429e03dc13fcedbfc70a0adcc28/privateconstructor.PNG)

```
HttpClientModule: C'est un module qui permet de faire des appels au back end

Observable: C'est comme des promesses en Node et avec un observable on utilise **.pipe** au lieu de **.then**

suscribe: nous permet de s'abonner au httpClient, il va aller se connecter au back office et récupérer la data 

pipe: sert à traiter les data qu'on a eu 

C'est interdit de faire les appels http dans un controleur et c'est aussi interdit de faire les suscribe dans les services 
```

Pour activer le cros origine ``cors`` pour quelqu'un il faut aller sur google et taper ``npmi cors``, on revient dans notre terminal et on se met dans
notre application back en utilisant les ``cd ou cd .. pour retourner`` et on tape ``npm i -S cors ``. Une fois que c'est installé on retourne dans google 
et on clique sur **Enable cors simple Usage** et on copie la variable ``var cors = require('cors');`` qu'on colle dans notre fichier ``index.ts``, 
on remplace var par const et on ajoute ``app.use(cors());``

![cors](/uploads/e45bb32a1b8886eda5daa6d162ccd145/cors.PNG)

![cors2](/uploads/dd6560eedaf41ea0c7da68dbaa223827/cors2.PNG)

Pour afficher sur notre navigateur  

Quand on a quelque chose qui renvoit un observable on doit mettre un ``§`` à la fin  

![Capture2](/uploads/1720479a5cdd3456015b7a209de41b63/Capture2.PNG)

![Capture3](/uploads/739720b1d1af5a0014957e040e596908/Capture3.PNG)

```
*ngFor: c'est une boucle for qui s'applique uniquement sur la partie HTML sur angular, il nous permet d'afficher les choses un par un 
```
 et pour ce faire on utilise les balises <ul> et  <li> comme suit:
 
![Capture4](/uploads/df74a558ce2012c5b7cde6f1e438a7aa/Capture4.PNG)

Quand on utilise un outil fourni par angular ou un outil en général il faut l'importer dans les modules comme suit 

![CaptureImport](/uploads/7d18a5e3a92f65e1c45567a94b4cc1cf/CaptureImport.PNG)

Et Que quand on utilise ces outils à l'intérieur des components ou des services il faut le mettre dans le constructor comme suit: 

![CaptureConstructor](/uploads/9450a9480c5f3447e132015dd6bb305d/CaptureConstructor.PNG)

Rappels:

Quand on veut appeler une liste on s'arrete à ``collection`` on va pas dans ``doc`` et après on utilise le ``forEach`` qu'il nous on donné
 
 ![Capture5](/uploads/9f0ef32018e00d78eb510b0a38df8d84/Capture5.PNG)
 
Comment faire un get 

![Capture6](/uploads/151e40c6408da36901e1108e20f45cfa/Capture6.PNG)

Comment utiliser un get en faisant des ``pipe`` et des ``tap``

![CaptureGet](/uploads/0b06a0f2af2e7e9b6dec329908fbb650/CaptureGet.PNG)

